[IOT STUDENT HOME](https://gitlab.com/iot110/iot110-student/blob/master/README.md)

## Setting up Lab1

### Objectives
For this lab we will be setting up a local web application running on the RPi
that will act as our primary user interface in a *headless* mode (i.e. not
requiring the HDMI graphic monitor connected to the RPi to interact with our
sensor and actuator devices.  The scope of this lab will ensure that we are
successful in connecting our networked RPi3 *Thing* controller, we are able to
log into the device, connect its file system to our development host (via sshfs)
and use the Atom editor to build our initial Python *Flask* Web Application.

The various steps of this lab are summarized as:
* Install Flask (lightweight Python Webserver)
* Ensure that we have ssh keys installed and sshfs operating smoothly.
* Build a Hello IoT WebApp using
* Run the Webserver from RPi3
* Test the Webserver from Browser and CLI

### Step 1: Getting Flask
```
pi$ pip install flask

```
### Step 2: Ensure ssh keys and sshfs operating smoothly
Secure shell (ssh) is the most universal and secure method to connect between
two computers, to transfer files (via the scp command) and to mount a
target's file system into the development host (via the sshfs command).

### Step 3: Build a Hello IoT App
Using the sshfs mounted folders and the Atom editor build the Flask demo
Web Application ```hello.py```.  This application will reply with a
simple HTML greeting page plus the hostname that was established during the
setup phase:

#### > Step 3a: Flask and socket
``` python
from flask import Flask
import socket
```

#### > Step 3b: Obtaining RPi Hostname from Socket Library
``` python
## Get my machine hostname
if socket.gethostname().find('.') >= 0:
    hostname=socket.gethostname()
else:
    hostname=socket.gethostbyaddr(socket.gethostname())[0]
```

#### > Step 3c: Create the Flash Webserver app
``` python
app = Flask(__name__)
```

#### > Step 3d: Establish the default "route"
``` python
@app.route("/")
def hello():
    return "Hello IoT World from RPi3: " + hostname
```

#### > Step 3e: Code to run server on port 5000
``` python
## Run the website and make sure to make
##  it externally visible with 0.0.0.0:5000 (default port)
if __name__ == "__main__":
    app.run(host='0.0.0.0')
```

### Start the webserver and run the hello.py app

```sh
pi$ python hello.py
 => runs the flask webserver from http://0.0.0.0, port 5000 for external access
```

### Test the hello.py WebApp from CLI and Remote Browser
```
pi$ curl http://0.0.0.0:5000
Hello IoT World from RPi3 (hostname) : iot8e3c.iot!

pi$ ifconfig | grep "inet addr"
 => inet addr:**192.168.10.19**  Bcast:192.168.10.255  Mask:255.255.255.0

host$ curl http://192.168.10.19:5000
=> Hello IoT World from RPi3 (hostname) : iot8e3c.iot!

host$ curl http://iot8e3c:5000
=> Hello IoT World from RPi3 (hostname) : iot8e3c.iot!
```

![HelloIoT](https://gitlab.com/iot110/iot110-student/raw/master/Resources/Images/Hello-IoT_416x288.png)


[IOT STUDENT HOME](https://gitlab.com/iot110/iot110-student/blob/master/README.md)
