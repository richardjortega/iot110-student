[LAB3 INDEX](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab3/setup.md)

### Part B - Adding SSE to the Webserver Code (main.py) ###
**Synopsis:** Flask has provisions built in to allow for streaming of data
from the server to clients.  We want to use that streaming capability to request
data from our GPIO driver and then send it through what Flask calls a ```Response```
mechanism.

#### Step B1: Timer
**Objective:** We will need to have a continuously running thread that updates
every half a second and sends updated state information on this endpoint.  
Therefore we will need to add the time library.  In addition we will use the
same library imports from the original web server implementation for GPIO.
```python
import time
from gpio import PiGpio
from debouncer import Debouncer
from flask import *

app = Flask(__name__)
pi_gpio = PiGpio()
db = Debouncer()
```

#### Step B2: SSE Endpoint
Next we need to add the routing endpoint code to read the switch status as well
as LEDs.  This endpoint will read the status of the switch, the 3 LEDs and then
build a data payload ```data: 0 1 0 1``` that will *yield* to the Flask Response
to communicate these data back to the web client. We will call this the myData
endpoint.  

```python
# =========================== Endpoint: /myData ===============================
# read the gpio states by GET method from curl for example
# curl http://iot8e3c:5000/myData
# -----------------------------------------------------------------------------
@app.route('/myData')
def myData():
    def get_state_values():
        while True:
            # return the yield results on each loop, but never exits while loop
            raw_switch = pi_gpio.read_switch()
            debounced_switch = str(db.debounce(raw_switch))
            led_red = str(pi_gpio.get_led(1))
            led_grn = str(pi_gpio.get_led(2))
            led_blu = str(pi_gpio.get_led(3))
            yield('data: {0} {1} {2} {3}\n\n'.format(debounced_switch,led_red,led_grn,led_blu))
            time.sleep(0.1)
    return Response(get_state_values(), mimetype='text/event-stream')
```

#### Step B3: SSE Client-side Subscriber
Now that we have a routing endpoint that creates an EventSource feed, we need a
client-side receiver of that data by matching up the method name with the
subscriber to that EventSource.  Below is a snippet of JavaScript that needs to
be at the bottom of index.html in order for it to receive data payloads.  We run
a simple test of this by logging output to the console.

```html
<script type="text/javascript">
  var switchSource = new EventSource("{{ url_for('myData') }}");
  switchSource.onmessage = function(e) {
    console.log(e.data);
  }
</script>
```

#### Step B4: Added Threaded Execution
Finally, because we want this thread to run independently from the other API
calls to this code, we need to simply tell the app.run method to ensure threaded
execution is enabled.  **Make sure this code is at the bottom of main.py**.

```python
if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True, threaded=True)
```

#### Step B5: Possible Errors with Threading
Whenever multiple threads are set up to run there's always the possibility that
the operating system will still be running that thread after killing the main
program.  For now, we will just manually kill these extra orphan threads by using
the Linux shell commands to list processes, obtain a process number and then to
kill off that extra process thread.  The symptom of this will be when you try to
run the main program again and then receive a *Traceback exception*.

Example:
```sh
pi$ python main.py
Traceback (most recent call last):
  File "main.py", line 82, in <module>
   ...
   socket.error: [Errno 98] Address already in use
```

To fix this we need to kill (using minus 9 for strong kill!) the orphan process.
```sh
pi$ ps aux | grep python
=> pi        3785  1.0  1.7  74788 16244 pts/0    Sl   18:15   0:04 /usr/bin/python main.py
=> pi        3900  0.0  0.2   4280  1908 pts/0    S+   18:22   0:00 grep --color=auto pytho
pi$ kill -9 3785

pi$ ps aux | grep python
=> pi        3902  0.0  0.1   4276  1876 pts/0    S+   18:22   0:00 grep --color=auto pytho
```

#### Step B6: Test from Chrome
Run through all of the states of the GPIO functionality by pulling up the
web developer console and observing the ```data: 0 1 0 1``` output that was
created from the console output above.

Now we are ready to start splitting off style and operations into their own
files also utilizing Bootstrap assets.

[PART C](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab3/PartC.md) Organize and Code Web Assets.

[LAB3 INDEX](https://gitlab.com/iot110/iot110-student/blob/master/Labs/Lab3/setup.md)
